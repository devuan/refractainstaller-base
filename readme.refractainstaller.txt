STARTING REFRACTA INSTALLER  (9.5.x)

*** NEW in 9.4 ***

- UEFI and BIOS installers have been merged (both gui and cli scripts)

- Installer supports gpt disk with bios boot. (special partition needed)

- Installer supports full-disk encryption (no separate /boot partition)



Refracta Installer will install the operating system to hard drive from
a running live-CD or live-USB session. The running system will be copied
exactly - any changes you make to the running system, including desktop
preferences, configuration changes, or added software will be copied to
the installed system.

You can run 'refractainstaller' from a terminal for the cli version.
For the graphical version, run Refracta Installer from the application
menu or start it from command line with 'refractainstaller-yad'.

See comments in /etc/refractainstaller.conf for various options that can
be set there.

Run pre-install scripts and Run post install scripts will enable
any scripts in /usr/lib/refractainstaller/pre-install or post-install
to run.

See /usr/lib/refractainstaller/installer_exclude.list if you want to
change the list of files/directories that won't be copied to the
installed system.  

Partitioning the disk(s) can be done before running the installer or from 
within the installer. There is no automatic partitioning. 
New feature: the installer will let you return to the partitioner before
proceeding, in case you want to change something.

If you are in a graphical environment, you can use gparted. If you are
not in a graphical environment, the installer will run either cfdisk or
gdisk (if a gpt partition table is detected.) 

If you want to use some filesystem other than ext2/3/4, pre-format your
partition(s) and select "Do not format" from the options menu. For the
text installer, set no_format="yes" in the config file.



INSTALLATION PROCEDURE

Start the installer.

Graphical installer opens a window with a checklist of options.
Text installer uses config file for options or else asks you.

  Change user name
  Create a new, separate /home partition
  Create a separate /boot partition
  Use existing swap partition (instead of swapfile.)
  Encrypt the root filesystem  (with or without separate /boot)
  Encrypt the /home partition (separate /home required)
  Write random data to encrypted partitions (more secure)
  Write zeroes to all partitions (to erase previous data)
  Do not format filesystems. (You can format them before running the installer.)
  Use UUID in etc/fstab and crypttab. (Useful if drive order changes.)
  Use filesystem labels (disk labels) in etc/fstab. (GUI installer only.)
  Change hostname.
  Disable automatic login to desktop.
  Disable automatic login to console. (Use stock Debian inittab)
  Move selected directories to separate partitions.
  Run pre-install scripts
  Run post-install scripts

Run the partitioner if you need to.
Choose partition(s) and filesystem format(s) for installation.
Enter information if requested (new host name, disk labels, 
passphrases for encryption)
Sit back and watch for a few minutes.
Enter new username and password, root password or choose sudo.
Select any directories you want moved to another partition.
Reboot into the new installation.



FULL-DISK ENCRYPTION

If you want your /boot directory to be encrypted, select encryption for
the root filesystem and DO NOT create or select a separate /boot partition.



CREATING ADDITIONAL SEPARATE PARTITIONS

If you chose to put /boot or /home on a separate partition, it (or they)
will be copied to the designated partition during the main part of the
installation.

If you want any directories other than /boot or /home on separate
partitions, for the gui installer, check "Move selected directories to 
separate partitions" in the expert options window, and for the text
installer, edit the config file to set additional_partitions="yes"

You must create and format the extra partitions during the partitioning
phase of the installation or before running the installer.

At the end of the installation, you will be asked to select the 
directories you want separate, and they will be copied to the selected
partitions. The contents of the original directory on the root filesystem
will then be deleted.



SPECIAL INSTRUCTIONS (for Devuan-Desktop-Live and Refracta isos)

For UEFI Install:
grub-efi-amd64 is installed. Near the end of the installation, you will be 
given a choice to install the bootloader or not. Be sure you have a proper
EFI partition: 50-500MB, FAT32, with esp flag in gparted or EF00 in gdisk.

For BIOS Install:
grub-pc package is in the root of the filesystem, and the installer will 
know what to do with it. Let the installer copy the package to /target 
and install the bootloader. You can also skip the bootloader if you
plan to use another.

For 32-bit grub with 64-bit system:
grub-efi-ia32 package is in the root of the file system. Install the
package before you run refractainstaller or install it to /target in
a chroot. The installer will pause to let you do this before it installs
the bootloader. It may also work if you let the installer copy the package
to /target during the installation.

For BIOS install with GPT partition table:
You will need to create a small partition (>1M), unformatted, with 
bios_grub flag in parted/gparted or EF02 in gdisk. Otherwise, you will 
need to boot from a disk that has dos partition table.
More info: http://www.rodsbooks.com/gdisk/bios.html"







